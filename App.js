import React from 'react';
import {
  AppRegistry, Easing, Animated, View, Text, Platform,YellowBox,TouchableOpacity,Dimensions
} from 'react-native';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation'
import SideMenu from './src/sideMenu'
import VetAnalise from './AppStack.js'
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader', 'Setting a timer for a long']);

const drawernav = createDrawerNavigator({
  Item1: {
      screen: VetAnalise,
    }
  }, {
    contentComponent: SideMenu,
    drawerWidth: Dimensions.get('window').width - 120,
});
// const VetAnalise = createStackNavigator({
//   TelaInicial: { screen: TelaInicialScreen },
//   Pressao: { screen: PressaoScreen },
//   Peso: { screen: PesoScreen },
//   ChatVet : { screen: ChatVetScreen },
//   Localizacao: { screen: LocalizacaoScreen },
//   Alimentacao: { screen: AlimentacaoScreen }
// },
//   { headerMode: 'float'},
//
// );


console.ignoredYellowBox = ['Warning: BackAndroid']


const App = createAppContainer(drawernav);

export default App;
