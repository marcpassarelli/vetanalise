import React from 'react';
import {
  AppRegistry, Text, Image,YellowBox,TouchableOpacity,Dimensions
} from 'react-native';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation'
import { SplashScreen } from './src/splash'
import { TelaInicialScreen } from './src/telaInicial'
import { VacinasScreen } from './src/vacinas'
import { PesoScreen } from './src/peso'
import { ChatVetScreen } from './src/chatVet'
import { LocalizacaoScreen } from './src/localizacao'
import { AlimentacaoScreen } from './src/alimentacao'
import { SaudeScreen } from './src/saude'
import SideMenu from './src/sideMenu'
import {cores} from './src/constants/constants'

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader', 'Setting a timer for a long']);


const VetAnalise = createStackNavigator({
  Splash: {screen: SplashScreen},
  TelaInicial: { screen: TelaInicialScreen,
    navigationOptions: ({navigation}) => ({
      title: "Microchip Animal",
      headerTitleStyle:{
        color: 'rgb(255,255,255)',
        fontFamily: 'Futura-Medium'
      },
      headerLeft:(
        <TouchableOpacity style={{}} onPress={() => navigation.openDrawer()}>
          <Image
                source={require('./chats-icon.png')}
                style={[{   width: 36,
            height: 36 }]}
              />
        </TouchableOpacity>
      ),
      headerStyle:{
        backgroundColor:cores.corDetails
      }
    }),
   },
  Peso: { screen: PesoScreen },
  ChatVet : { screen: ChatVetScreen },
  Vacinas : { screen: VacinasScreen },
  Localizacao: { screen: LocalizacaoScreen },
  Alimentacao: { screen: AlimentacaoScreen },
  Saude: { screen: SaudeScreen }
},
  { headerMode: 'float'},

);


console.ignoredYellowBox = ['Warning: BackAndroid']

export default VetAnalise;
