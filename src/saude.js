import React, { Component } from 'react';
import {Text,ScrollView } from 'react-native'
import { NavigationActions } from 'react-navigation';
import {cores} from './constants/constants'


export class SaudeScreen extends Component{
  static navigationOptions = ({navigation}) => ({
    headerMode:'float',
    title: 'Dicas de Saúde',
  })
constructor(props){
  super(props);

  this.state = {

  }


}

render() {

  console.ignoredYellowBox = [
    'Setting a timer'
  ]

    return (
      <ScrollView>
        <Text style={{marginHorizontal: 5,fontFamily: 'Futura-Medium',
        fontSize:24, alignSelf: 'center',textAlign: 'center',color: cores.corPrincipal,marginBottom: 20}}>6 dicas para cuidar da saúde do seu pet com sucesso</Text>
      <Text style={{marginHorizontal: 5,textAlign: 'justify',fontFamily: 'Futura-Medium',fontSize:16,marginBottom: 15}}>  Quem não gosta de ser recebido em casa com um sorriso, rabinho abanando e latidos de felicidade? Todo cachorro gosta de retribuir todo amor e carinho que recebe, mas é claro, além de um cafuné, eles também precisam de cuidados.
        Conheça algumas dicas interessantes para cuidar da saúde do seu pet e proporcione melhores dias ao seu amigo de quatro patas:</Text>

        <Text style={{marginHorizontal: 5,fontFamily: 'Futura-Medium',
        fontSize:20, alignSelf: 'center',textAlign: 'center',color: cores.corPrincipal,marginBottom: 15}}>Alimentação</Text>

        <Text style={{marginHorizontal: 5,textAlign: 'justify',fontFamily: 'Futura-Medium',fontSize:16,marginBottom: 15}}>  Pets que se alimentam muito rápido podem engasgar, vomitar, além de serem mais suscetíveis a problemas digestivos, como gastrite e má digestão. Algumas dicas envolvem utilizar comedouros lentos, que são divididos em compartimentos, o que diminui a ansiedade ao se alimentar.

Você também pode dividir a refeição em porções menores e oferecê-las de tempos em tempos ao longo do dia, o que evita que o animal se sinta faminto ao alimentar-se somente uma vez.</Text>

<Text style={{marginHorizontal: 5,fontFamily: 'Futura-Medium',
fontSize:20, alignSelf: 'center',textAlign: 'center',color: cores.corPrincipal,marginBottom: 15}}>Saúde bucal</Text>

<Text style={{marginHorizontal: 5,textAlign: 'justify',fontFamily: 'Futura-Medium',fontSize:16,marginBottom: 15}}>  Os pets também precisam de cuidados odontológicos para prevenir problemas como tártaro, mau hálito, gengivite e até a perda dos dentes. Por isso, observe sempre a coloração das gengivas de seu pet — que deve ser rosada —, faça a escovação com regularidade e inclua mordedores que auxiliem a saúde bucal.</Text>

  <Text style={{marginHorizontal: 5,fontFamily: 'Futura-Medium',
  fontSize:20, alignSelf: 'center',textAlign: 'center',color: cores.corPrincipal,marginBottom: 15}}>Hidratação</Text>

  <Text style={{marginHorizontal: 5,textAlign: 'justify',fontFamily: 'Futura-Medium',fontSize:16,marginBottom: 15}}>  Em relação à ingestão de água, para os animais maiores e principalmente nos dias mais quentes, é interessante deixar mais de um bebedouro pela casa. E não se esqueça de manter a água sempre fresca e limpa, pois além de ser mais saudável para o pet, muitos deles deixam de se hidratar quando a água não está recém-trocada.</Text>


    <Text style={{marginHorizontal: 5,fontFamily: 'Futura-Medium',
    fontSize:20, alignSelf: 'center',textAlign: 'center',color: cores.corPrincipal,marginBottom: 15}}>Higiene</Text>

    <Text style={{marginHorizontal: 5,textAlign: 'justify',fontFamily: 'Futura-Medium',fontSize:16,marginBottom: 15}}>  Animais de estimação necessitam de cuidados com a higiene que podem variar dependendo da raça, do tipo de pelagem, da saúde geral do animal, entre outros fatores. De modo geral, cães devem tomar banho uma vez por semana e gatos uma vez por mês.

No momento do banho, é primordial escolher os produtos corretos para o seu pet —que devem ser recomendados pelo veterinário. Além disso, os materiais devem ser separados anteriormente para facilitar todo o processo. Utilize água morna ou em temperatura ambiente, toalha, secador com ar frio, entre outros. Lembre-se de ter um cuidado especial com olhos e ouvidos.</Text>



      <Text style={{marginHorizontal: 5,fontFamily: 'Futura-Medium',
      fontSize:20, alignSelf: 'center',textAlign: 'center',color: cores.corPrincipal,marginBottom: 15}}>Roupas</Text>

      <Text style={{marginHorizontal: 5,textAlign: 'justify',fontFamily: 'Futura-Medium',fontSize:16,marginBottom: 15}}>  O uso de roupas pode ser indicado para o pet que tiver a pelagem mais curta, principalmente durante o inverno. Por isso, evite deixá-la molhada e troque-a sempre que necessário. Alguns animais podem caminhar com sapatilhas especiais, que protegem as patas da umidade, do frio e também de superfícies muito quentes.</Text>



        <Text style={{marginHorizontal: 5,fontFamily: 'Futura-Medium',
        fontSize:20, alignSelf: 'center',textAlign: 'center',color: cores.corPrincipal,marginBottom: 15}}>Ter um plano de saúde</Text>

        <Text style={{marginHorizontal: 5,textAlign: 'justify',fontFamily: 'Futura-Medium',fontSize:16,marginBottom: 15}}>  Com um plano de saúde você tem ampla rede credenciada, pois nem sempre é fácil carregar o companheiro para longe de casa, principalmente se o caso tem certa urgência. Com um bom seguro pet, seu animal terá acesso permitido em diversas clínicas e hospitais veterinários e com certeza terá um por perto!

O usuário conta com opções de cobertura mais abrangentes, planos veterinários oferecem possibilidades de cobertura que dão acesso aos principais exames e até mesmo a cirurgias, sem custos adicionais, além de descontos para mais de um animal. Nossos planos oferecem como benefício descontos para quem deseja adicionar mais de um pet. Ótimo para quem tem aquela família de quatro patas alegrando a casa!</Text>

<Text style={{marginHorizontal: 5,fontFamily: 'Futura-Medium',fontSize:12,marginBottom: 15}}>Fonte: https://www.meupetprotegido.com.br/dicas-e-curiosidades/6-dicas-para-cuidar-da-saude-do-seu-pet-com-sucesso</Text>

    </ScrollView>

    );
  }
}
