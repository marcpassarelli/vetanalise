import React, { Component } from 'react';
import {ScrollView,Text } from 'react-native'
import { NavigationActions } from 'react-navigation';
import { CheckBox } from 'react-native-elements'
import {cores} from './constants/constants'


export class VacinasScreen extends Component{
  static navigationOptions = ({navigation}) => ({
    headerMode:'float',
    title: 'Carteirinha de Vacinação',
  })
constructor(props){
  super(props);

  this.state = {

  }


}

render() {

  console.ignoredYellowBox = [
    'Setting a timer'
  ]

    return (
      <ScrollView>


        <CheckBox
          title='Múltipla canina (V8 ou V10)'
          checked={true}
        />
        <CheckBox
          title='Múltipla canina (1º reforço) (25/11/2019)'
          checked={false}
        />
        <CheckBox
          title='Giardíase (25/11/2019)'
          checked={false}
        />
        <CheckBox
          title='Múltipla canina (2º reforço) (23/12/2019)'
          checked={false}
        />
        <CheckBox
          title='Anti-Rábica (23/12/2019)'
          checked={false}
        />

</ScrollView>

    );
  }
}
