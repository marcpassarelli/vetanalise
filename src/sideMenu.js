
import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {NavigationActions} from 'react-navigation';
import {cores} from './constants/constants'
import {ScrollView, Text, View,TouchableOpacity,Image} from 'react-native';

class SideMenu extends Component {
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  render () {
    return (
      <View style={{paddingTop: 10,
    flex: 1}}>
    <View style={{flexDirection: 'row',justifyContent: 'space-between'}}>
      <Image
        source={require('../android/app/src/main/web_hi_res_512.png')}
        style={{
          borderRadius:wp('15%'),
          height:wp('30%'),
          width: wp('30%'),
          marginLeft: wp('15'),
          resizeMode: 'contain'
        }}/>
      <TouchableOpacity style={{alignSelf: 'center',}} onPress={() => this.props.navigation.closeDrawer()}>
      <Image
            source={require('../arrow-left.png')}
            style={[{   width: 32,
        height: 32,marginRight: 5}]}
          />
    </TouchableOpacity>
    </View>
    <Text style={{marginHorizontal: 5,fontFamily: 'Futura-Medium',
    fontSize:20, alignSelf: 'center',textAlign: 'center',color: cores.corPrincipal,marginVertical: 15}}>Alunos Responsáveis:</Text>
    <Text style={{marginHorizontal: 5,fontFamily: 'Futura-Medium',
    fontSize:16, alignSelf: 'center',textAlign: 'center',color: cores.corSecundaria,marginBottom: 15}}>Layse Rodrigues de Oliveira</Text>
    <Text style={{marginHorizontal: 5,fontFamily: 'Futura-Medium',
    fontSize:16, alignSelf: 'center',textAlign: 'center',color: cores.corSecundaria,marginBottom: 15}}>Patric Ramom Silva de Freitas</Text>
    <Text style={{marginHorizontal: 5,fontFamily: 'Futura-Medium',
    fontSize:16, alignSelf: 'center',textAlign: 'center',color: cores.corSecundaria,marginBottom: 15}}>Pedro Henrique de Souza</Text>
    <Text style={{marginHorizontal: 5,fontFamily: 'Futura-Medium',
    fontSize:16, alignSelf: 'center',textAlign: 'center',color: cores.corSecundaria,marginBottom: 15}}>Klebson Moura de Andrade</Text>
    <Text style={{marginHorizontal: 5,fontFamily: 'Futura-Medium',
    fontSize:16, alignSelf: 'center',textAlign: 'center',color: cores.corSecundaria,marginBottom: 15}}>Athais de Almeida Soares</Text>
    <Text style={{marginHorizontal: 5,fontFamily: 'Futura-Medium',
    fontSize:16, alignSelf: 'center',textAlign: 'center',color: cores.corSecundaria,marginBottom: 15}}>Igor Blosfeld de Oliveira</Text>
    <Text style={{marginHorizontal: 5,fontFamily: 'Futura-Medium',
    fontSize:16, alignSelf: 'center',textAlign: 'center',color: cores.corSecundaria,marginBottom: 15}}>Felipe Thiago de Souza</Text>
      </View>
    );
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object
};

export default SideMenu;

// <ScrollView style={{marginLeft: 5}}>
//       <Text style={{marginBottom: 10, fontSize: 16}} onPress={this.navigateToScreen('Peso')}>
//       Peso
//       </Text>
//       <Text style={{marginBottom: 10, fontSize: 16}} onPress={this.navigateToScreen('ChatVet')}>
//       Chat com o veterinário
//       </Text>
//       <Text style={{marginBottom: 10, fontSize: 16}} onPress={this.navigateToScreen('Localizacao')}>
//       Localização
//       </Text>
//       <Text style={{marginBottom: 10, fontSize: 16}} onPress={this.navigateToScreen('Alimentacao')}>
//       Alimentação
//       </Text>
// </ScrollView>
