console.ignoredYellowBox = [
    'Setting a timer'
]
import React, { Component } from 'react';
import { Text, Image, View,ImageBackground } from 'react-native';
import FadeInOutView from './fadeinoutview'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

/*import { Home } from './home'*/

export class SplashScreen extends Component {

  constructor(props) {
    super(props);
  }

  async getUser(){
    const { navigate } = this.props.navigation;
    navigate('TelaInicial')
  }

  componentDidMount() {
      setTimeout (() => {
        this.getUser()
      }, 3000);
  }

  static navigationOptions = {
     header: null,
  };

   render() {
     return(
       <FadeInOutView style={{marginLeft:wp('25%') ,justifyContent: 'center',alignContent: 'center',position: 'absolute'}}>
         <View>
           <Image
            style={{
              borderRadius:55, alignSelf:'center',
              height:hp('30%'),
              width: wp('50%'),
              marginTop: hp('28%'),
              resizeMode: 'contain'
            }}
            source={require('../android/app/src/main/web_hi_res_512.png')}
           />
         </View>
       </FadeInOutView>
     );
   }
}
