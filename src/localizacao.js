import React from 'react';
import { StyleSheet, Dimensions, View } from 'react-native';
import pedroImg from '../images/marker.png';
import MapView,{Marker} from 'react-native-maps';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = -3.218068;
const LONGITUDE = -52.220966;
const LATITUDE_DELTA = 0.0422;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;

const SAMPLE_REGION = {
  latitude: LATITUDE,
  longitude: LONGITUDE,
  latitudeDelta: LATITUDE_DELTA,
  longitudeDelta: LONGITUDE_DELTA,
};


export class LocalizacaoScreen extends React.Component{
  static navigationOptions = ({navigation}) => ({
    headerMode:'float',
    title: 'Localizacao',
  })

constructor(props){
  super(props);

  this.state = {
    latitude:LATITUDE,
    longitude: LONGITUDE
  }
}

componentDidMount(){
  //Espera os 3 segundos da animação e então chama a função para navegar até a tela de login

}


render() {

      return (
        <View style={StyleSheet.absoluteFillObject}>
          <MapView
            style={styles.map}
            initialRegion={SAMPLE_REGION}>
            <Marker
              title="Pedro está aqui"
              image={pedroImg}
              coordinate={{
                latitude: this.state.latitude,
                longitude: this.state.longitude
              }}
              anchor={{ x: 0.69, y: 1 }}
            />
          </MapView>
        </View>
      );
    }
}
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
});
