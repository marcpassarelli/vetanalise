import React, { Component } from 'react';
import {ScrollView,Text,Image,ImageBackground,View,TouchableOpacity } from 'react-native'
import { NavigationActions } from 'react-navigation';
import { BarChart, Grid,LineChart } from 'react-native-svg-charts'
import {cores,styles} from './constants/constants'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export class TelaInicialScreen extends Component{

  // static navigationOptions = {
  //   drawerLabel: 'Home',
  //   drawerIcon: ({ tintColor }) => (
  //     <Image
  //       source={require('../chats-icon.png')}
  //       style={[{ tintColor: tintColor,   width: 24,
  //   height: 24 }]}
  //     />
  //   ),
  // };


constructor(props){
  super(props);

  this.state = {

  }

}


render() {

  var dString = "September, 01, 2019";
  var d1 = new Date(dString);
  var d2 = new Date();

  console.ignoredYellowBox = [
    'Setting a timer'
  ]

  const fill = 'rgb(252, 204, 60)'
  const data = [50, 10, 40, 95, -4, -24, 0, 85, 0, 0, 35, 53, -53, 24, 50, -20, -80]

  return (
    <ScrollView style={{flex:1}}>
      <View style={{flexDirection: 'row',justifyContent: 'space-between',marginHorizontal: 6}}>
        <Image style={{
            borderWidth: 2,borderColor: cores.corSecundaria,
            width: wp('50%'),height: wp('50%'),alignSelf: 'center',marginTop: 5,borderRadius: wp('25%')
          }}source={require('../images/perfil.jpg')}/>

        <View style={{marginLeft: 5,marginTop: 5,alignSelf: 'center'}}>
        <View style={{flexDirection: 'row', marginBottom: 5}}>
          <Text style={styles.textTopic}>Pedro (macho)</Text>
        </View>
        <View style={{flexDirection: 'row', marginBottom: 5}}>
          <Text style={styles.textTopic}>Weimaraner</Text>
        </View>
        <View style={{flexDirection: 'row', marginBottom: 5}}>
          <Text style={styles.textTopic}>01/09/2019</Text>
        </View>
        <View style={{flexDirection: 'row', marginBottom: 5}}>
          <Text style={styles.textTopic}>{DateDiff.inMonths(d1, d2)} meses e {DateDiff.inDays(d1,d2)} dias</Text>
        </View>
        <View style={{flexDirection: 'row', marginBottom: 5}}>
          <Text style={styles.textTopic}>5 kg</Text>
        </View>
      </View>
      </View>
      <View style={{flexDirection: 'row',justifyContent: 'space-around',marginHorizontal: 10,marginTop:20,marginBottom: 10}}>
        <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Localizacao')}}>
          <Image source={require('../images/location.png')} style={styles.icons}>
          </Image>
          <Text style={styles.textIcons}>
            Localização
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>{this.props.navigation.navigate('ChatVet')}}>
          <Image source={require('../images/chat.png')} style={styles.icons}>
          </Image>
          <Text style={styles.textIcons}>
            Chat com Veterinário
          </Text>
        </TouchableOpacity>
      </View>

      <View style={{flexDirection: 'row',justifyContent: 'space-around',marginHorizontal: 10,marginBottom: 10}}>
        <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Saude')}}>
          <Image source={require('../images/saude.png')} style={styles.icons}>
          </Image>
          <Text style={styles.textIcons}>
            Dicas de Saúde
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Vacinas')}}>
          <Image source={require('../images/vaccine.png')} style={styles.icons}>
          </Image>
          <Text style={styles.textIcons}>
            Vacinas
          </Text>
        </TouchableOpacity>
      </View>




    </ScrollView>
  )
  }
}

var DateDiff = {

    inDays: function(d1, d2) {
        var t2 = d2.getTime();
        var t1 = d1.getTime();
        var days = parseInt((t2-t1)/(24*3600*1000))
        var result = days % 30

        return result;
    },

    inMonths: function(d1, d2) {
      var t2 = d2.getTime();
      var t1 = d1.getTime();
      var days = parseInt((t2-t1)/(24*3600*1000))
      var result = days / 30

        return parseInt(result);
    }
}

// <Text style={{marginTop: 10,alignSelf: 'center',fontSize: 24,color: '#472c82'}}>Gráfico de Barras</Text>
// <BarChart svg={{ stroke: 'rgb(252, 204, 60)' }} style={{ height: 200 }} data={data} svg={{ fill }} contentInset={{ top: 30, bottom: 30 }}>
//     <Grid
//        svg={{stroke: 'rgb(43, 189, 204)'}}/>
// </BarChart>
// <Text style={{alignSelf: 'center',fontSize: 24,color: '#472c82'}}>Gráfico de Linhas</Text>
//   <LineChart
//       style={{ height: 200 }}
//       data={data}
//       svg={{ stroke: 'rgb(252, 204, 60)' }}
//       contentInset={{ top: 20, bottom: 20 }}
//   >
//       <Grid svg={{stroke: 'rgb(43, 189, 204)'}}/>
//   </LineChart>
