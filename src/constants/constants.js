import {
  StyleSheet
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const cores = {
    corPrincipal: 'rgb(62, 64, 113)',
    corSecundaria: 'rgb(180, 155, 88)',
    corDetails:'rgb(54, 56, 104)',
};

export const styles = StyleSheet.create({
  titleTopic:{
    fontFamily: 'Futura-Medium',
    color: cores.corSecundaria,
    fontSize: wp('4%')
  },
  textTopic:{
    fontFamily: 'FuturaBT-MediumItalic',
  
    fontSize: wp('5.5%')
  },
icons:{
  width:wp('20%'),height:wp('20%'),
  resizeMode: 'contain',
  alignSelf: 'center'
},
textIcons:{
  fontSize: wp('4.5%'),
  alignSelf: 'center',
  fontFamily: 'Futura-Medium',
  color:cores.corSecundaria
}})
